<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\Category;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function welcome() {
        $announcements = Announcement::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->take(6)
        ->get();
        return view('welcome', compact('announcements'));
    }
    
    public function announcementsByCategory($name, $category_id){
        $category = Category::find($category_id);
        $announcements = $category->announcements()
        ->where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->paginate(6);
        
        return view('announcements.category', compact('category', 'announcements'));
        
    }
    
    public function show(Announcement $announcement)
    {
        //$categories = Category::all();
        
        return view('announcements.show',compact('announcement'));
    }
    
    
    
    public function search (Request $request) {
        $q = $request->input('q');
        
        $announcements = Announcement::search($q)->where('is_accepted', true)->get();
        //dd($annuncements);
        
        return view('search_result', compact('q', 'announcements'));
    }
    
    
    
    public function locale($locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }
    
}
