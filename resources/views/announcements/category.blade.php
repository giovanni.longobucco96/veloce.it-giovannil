@extends('layouts.app')
@section('content')

@include('includes._header')

<div class="container">
    <div class="row jusify-content-center">
        <div class="col-12 col-md-8">
            <h2>Annunci per categoria: {{ $category->name }}</h2>
        </div>
    </div>

    <div class="row">
        @foreach ($announcements as $announcement)
            @include('includes._announcements')
        @endforeach
    </div>


    <div class="row ">
        <div class="col-12 col-md-8">
            {{ $announcements->links() }}
        </div>
    </div>
</div>

@endsection