@extends('layouts.app')
@section('content')

<div class="container py-5">
    <div class="row">
        <div class="col-12">
            <h1 class="h3 fw-700">Ciao, Ecco l'elenco dei Richiedenti il ruolo di Revisore!</h1>
            <h3 class="h3 fw-700">Valuta Attentamente</h3>
        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-12  shadow bg-white mb-5">
            
            <table class="table table-over mt-3">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">
                            ID 
                        </th>
                        <th scope="col">
                            Nome
                        </th>
                        <th scope="col">
                            Email
                        </th>
                        <th scope="col">
                            Data Richiesta
                        </th>
                        <th scope="col">
                            IS ADMIN
                        </th>
                        <th scope="col">
                            IS Revisor
                        </th>
                        
                        <th scope="col">
                            make Revisor
                        </th>
                        <th scope="col">
                            refuse Revisor
                        </th>
                        <th scope="col">
                            View Profile
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php  
                    
                    $i = 0;
           
                    @endphp
                
                    @if ($newUsers[0] === "")
                        
                    @else
                            @if (count($newUsers) === 1)
                        
                            @php 
                                
                            
                            $date = new Datetime($requestList['created_at']);
                            $date = $date->format('d M Y');
                              
                            $newUsers = $newUsers[1];
                                
                            @endphp
                            <tr>
                                <td>{{$newUsers['id']}}
                                    <td>{{$newUsers['name']}}</td>
                                    <td>{{$newUsers['email']}}</td>
                                    <td>{{$date}}</td>
                                    <td>
                                        @if($newUsers['is_admin'])
                                        YESSA
                                        @endif
                                    </td>
                                    <td>
                                        @if($newUsers['is_revisor'])
                                        YESSA
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{route('admin.makeUserRevisor', $newUsers['id'])}}">Rendi untente revisore</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{route('admin.refuseUserRevisor', $newUsers['id']
                                        )}}">Rifiuta untente revisore</a>
                                    </td>
                                    <td>
                                        
                                        <a class="btn btn-primary" href="{{route('admin.user.profile', $newUsers['id'])}}">Vai Al Profilo</a>
                                    </td>
                                </tr>
                            
                            
                            @else
                            @foreach($newUsers as $newUser)
                            @php
                            $request = 0;
                            
                            $request = $requestList->where('user_email', $newUser['email']);
                            // $request = $request->map(function ($item, $key) {
                                //     return $item;
                                // });
                                
                                
                                $request =   json_decode(json_encode($request), true);
                                
                            
                                $thisrequest = $request[$i];
                                $date = new Datetime($thisrequest['created_at']);
                                $date = $date->format('d M Y');
                                // dd($request['created_at']);
                                // dd($date);
                                $i++;
                                @endphp
                                
                                <tr>
                                    <td>{{$newUser['id']}}
                                        <td>{{$newUser['name']}}</td>
                                        <td>{{$newUser['email']}}</td>
                                        <td>{{$date}}</td>
                                        <td>
                                            @if($newUser['is_admin'])
                                            YESSA
                                            @endif
                                        </td>
                                        <td>
                                            @if($newUser['is_revisor'])
                                            YESSA
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{route('admin.makeUserRevisor', $newUser['id'])}}">Rendi untente revisore</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{route('admin.refuseUserRevisor', $newUser['id'])}}">Rifiuta untente revisore</a>
                                        </td>
                                        <td>
                                            
                                            <a class="btn btn-primary" href="{{route('admin.user.profile', $newUser['id'])}}">Vai Al Profilo</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        @endsection