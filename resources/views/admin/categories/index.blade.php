@extends('layouts/app')

@section('content')

    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Categorie</h1>
                <a href="{{route('categories.create')}}" class="btn btn-dark my-3">Crea Nuova</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 shadow bg-white mb-5">
                <table class="table table-over mt-3">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">
                                ID 
                            </th>
                            <th scope="col">
                                Categoria
                            </th>
                            <th scope="col">
                                Shortcode
                            </th>
                            <th scope="col">
                                Thumb
                            </th>
                            <th scope="col">
                                Header
                            </th>
                            <th scope="col">
                                Modifica
                            </th>
                            <th scope="col">
                                Elimina
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                    <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->title}}</td>
                    <td>{{$category->shortTitle}}</td>
                    <td><img src="{{ Storage::url($category->categoryThumb) }}" alt="" class="img-fluid w-50"></td>
                    <td><img src="{{ Storage::url($category->categoryHeader) }}" alt="" class="img-fluid w-50"></td>
                    <td><a href="{{route('categories.edit', compact('category'))}}" class="btn btn-primary"> Modifica </a></td>
                    
                    
                    <td> <!-- Funzione delete all'interno della modale -->    
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modale_{{$category->id}}">Elimina</button>      
                    <div class="modal fade" id="modale_{{$category->id}}" tabindex="-1" aria-labelledby="label_modale" aria-hidden="true"> 
                    <div class="modal-dialog modal-dialog-centered"> 
                    <div class="modal-content">                            
                    <div class="modal-header">                                 
                    <h5 class="modal-title" id="label_modale">Elimina categoria</h5>                                 <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close"><span aria-hidden="true">&times;</span></button>     
                    </div>
                    <div class="modal-body">Sei sicuro di voler eliminare la categoria? L'operazione non è reversibile!</div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                    <form action="{{route('categories.delete', compact('category'))}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Elimina categoria<button>

                    </form>
                    </div>
                    </div>
                    </div>
                    </div>
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection