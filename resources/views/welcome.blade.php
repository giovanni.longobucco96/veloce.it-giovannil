@extends('layouts.app')
@section('content')

@include('includes._header')

@if (session('announcement.created.success'))
<div class="alert alert-success">
    annuncio creato correttamente, in attesa di revisione.
</div>
@endif

@if (session('access.deined.revisor.only'))
<div class="alert alert-danger">
    accesso consentito solo ai revisori
</div>
@endif


<div class="container">
    <div class="row">
        <div class="col-12">
            <div class=" form-group mb-4 mt-5"> 
                <h2 class=" text-center font-weight-bold ">CATEGORIE:</h2>
                @foreach ($categories as $category)
                
                <a id="categoriesDropdown"
                class=" nav-link  mx-1 my-1 btnCustom "
                role="button"
                href="{{route('announcement.category', [
                $category->name,
                $category->id
                ])}}"> 
                
                <div class="col-12 text-center" aria-labelledby="categoriesDropdown">
                    {{$category->name}}  
                </div> 
                </a> 
                @endforeach
            </div>
        </div>
    </div>
</div>


<div class="container mt-5 pt-5">
    <div class="row">
        <div class="col-12">
            <h2 class="text-uppercase text-center font-weight-bold">gli annunci più recenti:</h2>
        </div>
    </div>
</div>


<div class="container  my-3 py-3">
    
    
    <div class="row ">
        <div class="swiper-container h-100 w-100">
            <div class="swiper-wrapper">
       
            @foreach ($announcements as $announcement)

                @include('includes._announcements')
            
            
            @endforeach
        
        </div>
        <!-- Add Scrollbar -->
        <div class="swiper-scrollbar"></div>
    </div>
        
    </div>
    
@endsection
    
    
