@extends('layouts.app')

@section('content')
<div class="container main-padding-custom">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card neonCard h-100 w-100" >
                <div class="card-header"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h2 class="text-center">{{ __('You are logged in!') }}</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
