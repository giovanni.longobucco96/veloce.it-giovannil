<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AnnouncementRequest;
use App\Jobs\ResizeImage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function newAnnouncement(Request $request)

    {
        $uniqueSecret = $request->old(
            'uniqueSecret', 
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
        return view('announcements.new', compact('uniqueSecret'));    
    }

    public function create(AnnouncementRequest $request)
    {
        $a = new Announcement();

        $a->title = $request->input('title');
        $a->description = $request->input('description');
        $a->price = $request->input('price');
        $a->category_id = $request->input('category');
        $a->user_id = Auth::user()->id; 

        $a->save();

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}");
        $removedImages = session()->get("removedImages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        foreach ($images as $image){
            $i = new AnnouncementImage();

            $fileName = basename($image);
            $newFileName = "public/announcements/{$a->id}/{$fileName}";
            Storage::move($image,  $newFileName );

            dispatch(new ResizeImage(
                $newFileName,
                250,
                250
            ));
            
            dispatch(new ResizeImage(
                $newFileName,
                500,
                500
            ));
           

            $i->file = $newFileName;
            $i->announcement_id = $a->id;

            $i->save();
        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));


        
        return redirect('/')->with('announcement.created.success', 'ok');
    }

    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));

        session()->push("images.{$uniqueSecret}", $fileName);

       return response()->json(
           [
               'id' => $fileName
           ]
        );

    }

    public function removeImage (Request $request){
                        
                        
        $uniqueSecret = $request->input('uniqueSecret');
        
        $fileName = $request->input('id');
        session()->push("removedImages.{$uniqueSecret}", $fileName);
        Storage::delete($fileName);
        return response()->json('ok');
    }

    public function getImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach($images as $image) {
            $data[] = [
                'id' => $images,
                'src'=> AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }

        return response()->json($data);
    }

}
