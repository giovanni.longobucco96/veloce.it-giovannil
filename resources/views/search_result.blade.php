@extends('layouts.app')
@section('content')

@include('includes._header')

<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 class="text-uppercase pt-5">I risultati della tua ricerca:</h2>
      <hr class="bg-secondary-color">
    </div>
  </div>
</div>

<div class="container ">
    <div class="row">
      @foreach ($announcements as $announcement)
        @include('includes._announcements')
        @endforeach
    </div>
</div>



@endsection