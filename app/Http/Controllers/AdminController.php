<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\RevisorRequest;
use Illuminate\Http\Request;

class AdminController extends Controller
{
  public function makeUserRevisor($user){
    $user = User::find($user);
   

      $request = RevisorRequest::where('user_id', $user);
      
      $user->is_revisor = true;
      $request->delete();
      $user->save();
      return redirect()->back()->with('message', "L'utente $user->name è ora Revisore");
    

  }
  public function refuseUserRevisor($user){
    
    $request = RevisorRequest::where('user_id', $user);
    $user = User::find($user);
    $user->is_revisor = false;
    $request->delete();
    $user->save();
    return redirect()->back()->with('message', "L'utente $user->name è ora Revisore");

  }

  public function userList()
  {
      $users = User::all();
      return view('admin.users.listall', compact('users'));
  }

  public function userRequestList()
  {
    $requestList = RevisorRequest::all();
    $newUsers = [0 => ''];
  
    if(count($requestList) > 1) {

      foreach($requestList as $request) {

        $users = User::where('email', $request->user_email)->get();
        $users = json_decode(json_encode($users), true);

        
        $newUsers = array_filter(array_merge($newUsers, $users));
            
      }
         
  
    }
     elseif (count($requestList) > 0) {
      $requestList = json_decode(json_encode($requestList), true);
      $requestList = $requestList[0];
  
      $users = User::where('email', $requestList['user_email'])->get();
      $users = json_decode(json_encode($users), true);

      
      $newUsers = array_filter(array_merge($newUsers, $users));
    }
    else {

    }



    return view('admin.users.revisorRequests', compact('newUsers','requestList'));
  }

  public function userProfile($user) {
    
    $user = User::find($user);

    return view ('admin.users.profile', compact('user'));
  }
  
}
