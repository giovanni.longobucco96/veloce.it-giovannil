


<nav id="navbar" class="navbar navbar-expand-lg bg-main-color  py-3 position-fixed w-100">
    <a class="navbar-brand" href="#"><i id="logoCustom" class="fas fa-rocket fa-2x text-ascent-color "></i> </a>
    <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarCustom" aria-controls="navbarCustom" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-align-right text-ascent-color"></i>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarCustom">
        <ul class="navbar-nav mr-auto">
            
            <li class=" nav-item nav-hover">
                <a id="btnNavWelcome" class="nav-link text-ascent-color font-weight-bold text-uppercase " href="{{ route('welcome')}}">welcome <span class="sr-only">(current)</span></a>
            </li>
            
            <li class=" nav-item nav-hover dropdown bg-green ">
                <a class="nav-link text-ascent-color font-weight-bold text-uppercase" href="{{ route('announcement.new')}}">Nuovo Annuncio</a>
            </li>
            
            <li class="nav-item">
                @include('includes._locale', ['lang' => 'it', 'nation' => 'it'])
            </li>
            
            
            <li class="nav-item">
                @include('includes._locale', ['lang' => 'en', 'nation' => 'gb'])
            </li>
            
            
            <li class="nav-item">
                @include('includes._locale', ['lang' => 'es', 'nation' => 'es'])
            </li>

            
        </ul>
        
        <ul>
            <li>
                
                <form action="{{ route('search')}}" method="GET" class="pt-2 ">
                    <input type="text" name="q" style="width: 200px;" placeholder="Search">
                    <button class="btn font-weight-bold btnCustom " type="submit">Ricerca</button>
                </form>
            </li>
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            
            
            
            @guest
            <li>
                <i id="btnNavFontLogin" class=" text-ascent-color fas fa-user-circle fa-2x my-2 glow"></i>
            </li>
            <li class="nav-item">
                <a id="btnNavLoginCust" class="nav-link  text-ascent-color  font-weight-bold text-uppercase glow" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
                <a id="btnNavRegisterCust" class="nav-link  text-ascent-color glow font-weight-bold text-uppercase" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif
            @else
            
            @if(Auth::user()->is_admin)
            <li class="nav-item">
                <a class="nav-link text-ascent-color" href="{{route('admin.users.list')}}">
                    admin home
                    <span class="badge badge-pill badge-warning">{{\App\RevisorRequest::toBeCheckCount()}}</span>
                </a>
            </li>
            @endif
            
            @if(Auth::user()->is_revisor)
            <li class="nav-item">
                <a class="nav-link text-ascent-color" href="{{route('revisor.index')}}">
                    revisor home
                    <span class="badge badge-pill badge-warning">{{\App\Models\Announcement::ToBeRevisionedCount()}}</span>
                </a>
            </li>
            @endif
            
            
            <li class="nav-item dropdown ">
                <a id="navbarDropdown" class="nav-link text-ascent-color dropdown-toggle " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                
                
                
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        @endguest
    </ul>
</div>
</nav>

