@extends('layouts.app')
@section('content')



<div class="container  main-padding-custom">
    <div class="row  justify-content-between">
        <div class="col-12 col-md-3 pt-2 ">
            
            
            
            <div class="swiper-container h-100 w-100">
                <div class="swiper-wrapper">
            
                        @foreach ($announcement->images as $image)
                        
                            <div class="swiper-slide">
                                
                                <img src="{{ $image->getUrl() }}" class="pt-2 px-2 card-img-top rounded " alt="">
                                
                            </div> 
                        
                        @endforeach
                    
                </div>
                <!-- Add Scrollbar -->
                <div class="swiper-scrollbar"></div>
            </div>
            
            
            <h3 class="text-ascent-color card-text mt-5">Prezzo: {{$announcement->price}}€</h3>
        </div>
        
        <div class=" mt-5 col-12 col-md-6 text-center text-uppercase">
            <h2>{{$announcement->title}}</h2>
            
            <hr class="bg-secondary-color">
            
            <p class="text-ascent-color text-left">{{$announcement->description}}</p>
            
            <hr class="bg-secondary-color">
            
            <strong class="text-ascent-color">caregory: <a  href="{{route('announcement.category', [
                $announcement->category->name,
                $announcement->category->id])}}">
                
                {{$announcement->category->name}}</a> </strong> <br>
                <i class="text-ascent-color">{{$announcement->created_at->format('d/m/y')}} - {{ $announcement->user->name }}</i>
            </div>
            
        </div>
    </div>
    
    @endsection
    
    
    
    