


<div class="col-12 col-md-6 col-lg-4 my-5 swiper-slide  ">
 
    <div class="card neonCard h-100">
        @php
       
        $image = $announcement->images->sortDesc()->pop();  
        @endphp 
        @if ($image)
        
      
        <img src="{{ $image->getUrl(250,250) }}" class=" rounded img-fluid " alt="">
        
        @else

        <img src="https://via.placeholder.com/300x150" class=" rounded img-fluid " alt="">

        @endif

        <div class="card-body">
          <h5 class="card-title">{{$announcement->title}}</h5>
          <p class="card-text">{{$announcement->price}}€</p>

        
          <strong>caregory: <a href="{{route('announcement.category', [
            $announcement->category->name,
            $announcement->category->id])}}">
            
          {{$announcement->category->name}}</a> </strong> <br>
            <i>{{$announcement->created_at->format('d/m/y')}} - {{ $announcement->user->name }}</i>
            
          </div>
          <div class="card-footer ">
          <a href="{{route('announcement.show', compact('announcement'))}}" role="button" class="float-right btnCustom" style="text-decoration: none;" >visualizza</a>
        </div>
      </div>

</div>