@extends('layouts.app')
@section('content')

@if($announcement)

<div class="container main-padding-custom ">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card neonCard h-100 w-100">
            <div class="card-header"><h2>Annuncio # {{$announcement->id}}</h2></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2"><h3>Utente</h3></div>
                    <div class="col-md-10">
                        # {{$announcement->user->id}},
                        {{$announcement->user->name}},
                        {{$announcement->user->email}},
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-2"><h3>Titolo</h3></div>
                    <div class="col-md-10">{{$announcement->title}}</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-2"><h3>Descrizione</h3></div>
                    <div class="col-md-10">{{$announcement->description}}</div>
                </div>
                <div class="row">
                    <div class="col-md-2"><h3>Immagini</h3></div>
                    <div class="col-md-10">
                        <div class="row mb-2">
                           
                                @foreach ($announcement->images as $image)
                                <div class="col-md-4">
                                    <img src="{{ $image->getUrl(500,500) }}" class=" rounded img-fluid " alt="">
                                    
                                </div>
                                @endforeach
                            
                            <div class="col-md-8">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mt-5">
        <div class="col-md-6">
            <form action="{{route('revisor.reject', $announcement->id)}}" method="POST">
            @csrf
                <button type="submit" class="btn btn-danger text-uppercase font-weight-bold">rifiuta annuncio</button>
            </form>
        </div>
        <div class="col-md-6 ">
            <form action="{{route('revisor.accept', $announcement->id)}}" method="POST">
            @csrf
                <button type="submit" class="btn btn-success text-uppercase font-weight-bold">Accetta annuncio</button>
            </form>
        </div>
    </div>
</div>

@else
<div class="container">
    <div class="row justify-content-center ">
        <div class="col-12 col-md-6 ">
            <h2 class=" text-uppercase text-center mx-auto tfont-weight-bold my-5 py-5">grande, hai finito il tuo lavoro!!! <br> non ci sono annunci da revisionare </h2>
            
        </div>
    </div>
</div>
@endif


@endsection