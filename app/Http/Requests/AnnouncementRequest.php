<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'nome proprietà' => 'regole'
            'title' => 'required|max:30|min:5',
            'description' => 'required|min:10',
        ];
    }

    public function messages()
    {
        return [
            // 'nome propiretà.nome regola' => 'messaggio'
            'title.required' => 'Il titolo è obbligatorio',
            'title.max' => 'Il titolo deve avere massimo 30 caratteri(compresi spazi)',
            'title.min' => 'Il titolo deve avere minimo 5 caratteri(compresi spazi)',
            'description.required' => 'la descrizione è obbligatoria',
            'description.min' => 'la descrizione deve avere minimo 10 caratteri(compresi spazi)'
        ];
    }
}
