@extends('layouts.app')
@section('content')
<h1 class="text-center my-3 display-4 fw-700">ADMIN USER LIST </h1>
<div class="container">
    <div class="row">
        <div class="col-12 shadow bg-white mb-5">
            <table class="table table-over mt-3">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">
                            ID 
                        </th>
                        <th scope="col">
                            Nome
                        </th>
                        <th scope="col">
                            Email
                        </th>
                        <th scope="col">
                            IS REVISOR
                        </th>
                        <th scope="col">
                            IS ADMIN
                        </th>
                        <th scope="col">
                            PENDING REVISOR REQUST
                        </th>
                        <th scope="col">
                            make Revisor
                        </th>
                        <th scope="col">
                            View Profile
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    @php
                    
                    
                    @endphp
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if ($user->is_revisor === 1)
                            <h5>è un grande revisore</h5>
                            @else 
                            <h5>Non è un revisore </h5>
                            @endif
                        </td>
                        <td>
                            @if ($user->is_admin === 1)
                            <h5>è un grande Admin</h5>
                            @else 
                            <h5>Non è un ADMIN </h5>
                            @endif
                            
                        </td>
                        
                        @php
                        $check = \App\RevisorRequest::where('user_email', $user->email)->get();
                        
                        
                        @endphp
                        
                        
                        
                        @if (count($check) > 0)
                        <td> Ha una richiesta Pendente </td>
                        @else 
                        <td> NON Ha una richiesta Pendente </td>
                        @endif
                        
                        <td>
                            <a class="btn btn-primary" href="{{route('admin.makeUserRevisor', $user->id
                            )}}">Rendi Revisore</a>
                        </td>
                        <td>
                            
                            <a class="btn btn-primary" href="{{route('admin.user.profile', $user->id)}}">Vai Al Profilo</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
