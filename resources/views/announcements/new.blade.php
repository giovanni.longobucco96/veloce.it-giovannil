@extends('layouts.app')
@section('content')

<div class="container  main-padding-custom">
    <div class="row">
        <div class="col-12 col-md-12 ">
            <div class="card neonCard  h-100 w-100">
                
                <div class="card-header text-uppercase"><h2 class="text-center mt-3">inserisci il tuo annuncio</h2></div>
                
                <hr class="bg-secondary-color">
                
                <div class="card-body">
                    <h3>DEBUG:: SECRET{{$uniqueSecret}}</h3>
                    <form action="{{ route('announcement.create')}}" method="POST">
                        @csrf 
                        
                        <input 
                            type="hidden"
                            name="uniqueSecret"
                            value="{{ $uniqueSecret}}">

                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-from-label text-md-right"><p>Categoria</p></label>
                            
                            <div class="col-md-6">
                                <select name="category" id="category">
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}"
                                        {{old('category') == $category->id ? 'selected' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                    @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                                    
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="title" class="col-12 col-md-4 col-form-label text-md-right"><p>Titolo</p></label>
                                
                                <div class="col-md-6">
                                    <input 
                                    type="text" id="title"
                                    class="form-control @error('title') is-invalid @enderror"
                                    name="title" value="{{old('title')}}" required autofocus>
                                    
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="description" class="col-12 col-md-4 col-form-label text-md-right"><p>Descrizione</p></label>
                                
                                <div class="col-md-6">
                                    <textarea id="description"
                                    class="form-control @error('description') is-invalid @enderror"
                                    name="description" required autofocus 
                                    cols="30" rows="10">{{old('description')}}</textarea>
                                    
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="price" class="col-12 col-md-4 col-form-label text-md-right"><p>Prezzo</p></label>
                                
                                <div class="col-md-6">
                                    <input 
                                    type="float" id="price"
                                    class="form-control @error('price') is-invalid @enderror"
                                    name="price" value="{{old('price')}}" required autofocus>
                                    
                                    @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            
                            
                            <div class="form-group row">
                                <label for="images" class="col-md-12 col-form-label text-md-left ">
                                    immagini
                                </label>
                                <div class="col-md-12">
                                    <div class="dropzone" id="drophere"></div>
                                    @error('images')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                    
                                    @enderror
                                </div>
                            </div>
                            
                            
                            <button type="submit" class="btnCustom float-right"> Crea </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    