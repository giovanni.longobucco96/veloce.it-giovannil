<?php

use App\Http\Controllers\FrontController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RevisorController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROTTA WELCOME
Route::get('/', [FrontController::class, 'welcome'])->name('welcome');

//ROTTA SHOW DEL SINGOLO ANNUNCIO
Route::get('/annunci/dettaglio/{announcement}',[FrontController::class, 'show'])->name('announcement.show');

// ROTTA ANNUNCI PER CATEGORIE
Route::get('/category/{name}/{id}/announcements', [FrontController::class, 'announcementsByCategory'])->name('announcement.category');





//ROTTA SEARCH
Route::get('/search', [FrontController::class, 'search'])->name('search');




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/annunci/new', [HomeController::class, 'newAnnouncement'])->name('announcement.new');
Route::post('/annunci/salvaannuncio', [HomeController::class, 'create'])->name('announcement.create');
Route::get('/annunci/dettaglio/{announcement}',[FrontController::class, 'show'])->name('announcement.show');

Route::get('/annunci/images', [HomeController::class, 'getImages'])->name('announcement.images');

Route::post('/annunci/images/upload', [HomeController::class, 'uploadImage'])->name('announcement.images.upload');

Route::delete('/annunci/images/remove', [HomeController::class, 'removeImage'])->name('announcement.images.remove');

// ROTTE REVISORE

Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisor.index')->middleware('auth.revisor');

Route::post('/revisor/annuncement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');

Route::post('/revisor/annuncement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');

//ROTTA MULTILINGUA
Route::post('/locale/{locale}', [FrontController::class, 'locale'])->name('locale');